<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'world' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '4Ed(T2Ew4(E`Y94K0(.yFM_@;!%[1AS+_b:JO_&T~{6?$.,jg)MPzpe`=i,M_7d=' );
define( 'SECURE_AUTH_KEY',  ',^;5sdo;Ts5Eg5i&|]]hS?!]1`l+nqK,%zaco/*_zwFI_]w>;;1.5w8p4n3(Z|g_' );
define( 'LOGGED_IN_KEY',    '~U~C>@0`s?pL{CFa3tF4w3,qmm<]=O,2!ZLOVWB]S16-:Bat+swK&K|yvi`+r>t;' );
define( 'NONCE_KEY',        'TQQ|C7RtbgpuHo+Z,lZ+E7 #c1bb w%JR*65({K2Zq?1x:oqU3}xp:D ==5T`GHN' );
define( 'AUTH_SALT',        'FKIeDI1kT$Y4jk@2{.JtJh;&(:(EZhyfX@(<y>&uPfRtHZ^@QkeqcTF#)Tw5Te&_' );
define( 'SECURE_AUTH_SALT', 'o]4w6D&w|3`l1`<~U8gm>P0:Oq<>]gE-A);H3Q@i;p![Rq18b&~f;{JB9aXj,7eR' );
define( 'LOGGED_IN_SALT',   'gS5tw8n&ge4~QGBUXVcPV,bI3gKBuC#bp0Q8l0yve5m*)QhRno)2n-,PUABd&RU+' );
define( 'NONCE_SALT',       '>{Hk}gnj}%._9A%25(Sf<iwMcX!w+!J4Y9C^^@eiX|7&Fs2i@;=v42_Sa7s^?xs#' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
