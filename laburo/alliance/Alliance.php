<?php

$u = 'aseguronline';
$p = 'cK2cfa1s9dc5i';
$userName = 'aseguronline';
$AppName = 'AseguroOnline';
$pais = 'ARG';
$target = 'Allianz';
$codAcc = '';
$nroPag = '1';
$cantReg = '15';
$accesorios =
'<cot:ListaAccesorios>
    <cot:AccesorioVehiculo>
        <cot:codigoAccesorio>20</cot:codigoAccesorio>
        <cot:sumaAccesorio>1500</cot:sumaAccesorio>
    </cot:AccesorioVehiculo>
</cot:ListaAccesorios>'
;
$condiciones=
'<con:CondicionesContratacion>
  <con:tipoDePoliza>M</con:tipoDePoliza>
  <con:medioDePago>T</con:medioDePago>
  <con:cantidadDeCuotas>1</con:cantidadDeCuotas>
  <con:codigoCondicionIVA>1</con:codigoCondicionIVA>
  <con:codigoCondicionIIBB>1</con:codigoCondicionIIBB>
  <con:tipoDocumentoTomador>D</con:tipoDocumentoTomador>
  <con:numeroDocumentoTomador>30789385</con:numeroDocumentoTomador>
  <con:fechaDesde>2018-10-01T00:00:00.000-03:00</con:fechaDesde>
  <con:fechaHasta>2018-10-31T00:00:00.000-03:00</con:fechaHasta>
  <con:fechaNacimientoAsegurado>1985-07-05T00:00:00.000-03:00</con:fechaNacimientoAsegurado>
  <con:sexoDelAsegurado>H</con:sexoDelAsegurado>
</con:CondicionesContratacion>'
;
$adicionales=
'<cot:ListaAdicionales>
  <cot:Adicional>
      <cot:codigoDeAdicional>001</cot:codigoDeAdicional>
  </cot:Adicional>
</cot:ListaAdicionales>'
;
$riesgo=
'<cot:UbicacionDelRiesgo>
  <cot:codigoPostal>1005</cot:codigoPostal>
  <cot:codigoProvincia>0</cot:codigoProvincia>
<cot:codigoZonaDeRiesgo/>
</cot:UbicacionDelRiesgo>'
;

$codProductor='M5127';
$codMarcaModelo='460540';
$fecFab='2012';
$valorVehic='0';
$codUso='1';
$es0='false';
$conAlarma='false';

$xml = '<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope
    xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:cot="http://xmlns.allianz.com.ar/Core/EBM/Vehiculo/CotizacionVehiculo"
    xmlns:ebm="http://xmlns.allianz.com.ar/CommonCore/EBM"
    xmlns:con="http://xmlns.allianz.com.ar/Core/EBO/Allianz/CondicionesContratacion">
    <soapenv:Header>
    <user>' . $u . '</user>
    <pwd>' . $p . '</pwd>
    </soapenv:Header>
    <soapenv:Body>
        <cot:CalcularCotizacionFullVehiculoEBM>
            <ebm:EBMHeader>
                <ebm:Sender>
                <ebm:userName>' . $userName . '</ebm:userName>
                <ebm:Application>' . $AppName . '</ebm:Application>
                <ebm:Country>' . $pais . '</ebm:Country>
                </ebm:Sender>
                <ebm:Target>' . $target . '</ebm:Target>
            </ebm:EBMHeader>
            <cot:DataArea>
                <cot:CalcularCotizacionFullVehiculo>
                $codProductor=M5127</cot:codigoDeProductor>
                <cot:codigoDeProductor>' . $codProductor . '</cot:codigoDeProductor>
                <cot:VehiculoACotizar>
                $codMarcaModelo=460540</cot:codigoMarcaModelo>
                    <cot:codigoMarcaModelo>' . $codMarcaModelo . '</cot:codigoMarcaModelo>
                    $fecFab2012</cot:anioFabricacion>
                    <cot:anioFabricacion>' . $fecFab . '</cot:anioFabricacion>
                    $valorVehic0</cot:valorVehiculo>
                    <cot:valorVehiculo>' . $valorVehic . '</cot:valorVehiculo>
                    $codUso1</cot:codigoDeUso>
                    <cot:codigoDeUso>' . $codUso . '</cot:codigoDeUso>
                    $es0false</cot:es0Km>
                    <cot:es0Km>' . $es0 . '</cot:es0Km>
                    $conAlarma=false</cot:tieneAlarma>
                    <cot:tieneAlarma>' . $conAlarma . '</cot:tieneAlarma>
                    ' . $accesorios . '
                </cot:VehiculoACotizar>
                ' . $condiciones . '
                ' . $adicionales . '
                ' . $riesgo . '
                </cot:CalcularCotizacionFullVehiculo>
            </cot:DataArea>
        </cot:CalcularCotizacionFullVehiculoEBM>
    </soapenv:Body>
</soapenv:Envelope>';

$location_URL = 'https://wbs.allianzonline.com.ar:9443/Vehiculo/Externo/Atributos/AtrVehiculoExtReqABCS?wsdl';

$client = new SoapClient(null, array(
    'location' => $location_URL,
    'uri' => $location_URL,
    'trace' => 1,
    // 'proxy_host' => "proxy01.aysa.ad",
    // 'proxy_port' => 80,
    // 'proxy_login' => "AYSA\I0603064",
    // 'proxy_password' => "Mayo2019",
));

$search_result = $client->__doRequest($xml, $location_URL, $location_URL, 1);

$xml = new SimpleXMLElement(($search_result));

echo $xml->asXML();
//$xml->registerXPathNamespace('soapenv', 'http://schemas.xmlsoap.org/soap/envelope/');
//$xml->registerXPathNamespace('atr', 'http://xmlns.allianz.com.ar/Core/EBM/Vehiculo/AtrVehiculo');
//$xml->registerXPathNamespace('ebm', 'http://xmlns.allianz.com.ar/CommonCore/EBM');
// $xml->registerXPathNamespace('acc', 'http://xmlns.allianz.com.ar/Core/EBO/Allianz/AccesorioVehiculo');
$xml->registerXPathNamespace('cot1', 'http://xmlns.allianz.com.ar/Core/EBO/Allianz/CotizacionFullVehiculo');

// $resultado = $xml->xpath('//acc:AccesorioVehiculo');
$resultado = $xml->xpath('//cot1:CotizacionFull');

//$nodeCount = count($resultado);

$i = 0;
foreach ($xml->xpath('//cot1:CotizacionFull') as $resultado) {
    // echo " codigo: ";
    // echo $resultado->xpath('//acc:codigoAccesorio')[$i];
    // // echo " descr: ";
    // echo $resultado->xpath('//acc:descripcionAccesorio')[$i];

    echo $resultado->xpath('//cot1:codigoDeCobertura')[$i];
    echo $resultado->xpath('//cot1:descripcionDeCobertura')[$i];
    echo $resultado->xpath('//cot1:codigoDeProducto')[$i];
    echo $resultado->xpath('//cot1:descripcionDeProducto')[$i];

    echo "\n\n";
    $i++;

}
