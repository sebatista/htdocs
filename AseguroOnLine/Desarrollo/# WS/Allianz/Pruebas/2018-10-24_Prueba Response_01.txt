<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <cot:CalcularCotizacionFullVehiculoResponseEBM xmlns:cot="http://xmlns.allianz.com.ar/Core/EBM/Vehiculo/CotizacionVehiculo">
         <ebm:EBMHeader xmlns:ebm="http://xmlns.allianz.com.ar/CommonCore/EBM">
            <ns0:EBMID xmlns:ns0="http://xmlns.allianz.com.ar/CommonCore/EBM">03d201ac-8bf1-496f-828b-171abd8f180f</ns0:EBMID>
            <ns0:CreationDateTime xmlns:ns0="http://xmlns.allianz.com.ar/CommonCore/EBM">2018-10-29T15:00:16.624-03:00</ns0:CreationDateTime>
            <ns0:Sender xmlns:ns0="http://xmlns.allianz.com.ar/CommonCore/EBM">
               <ns0:userName>aseguronline</ns0:userName>
               <ns0:Application>AseguroOnline</ns0:Application>
               <ns0:Country>ARG</ns0:Country>
            </ns0:Sender>
            <ns0:Target xmlns:ns0="http://xmlns.allianz.com.ar/CommonCore/EBM">Allianz</ns0:Target>
         </ebm:EBMHeader>
         <ebm:ReturnCode xmlns:ebm="http://xmlns.allianz.com.ar/CommonCore/EBM">0</ebm:ReturnCode>
         <ebm:ReturnMessage xmlns:ebm="http://xmlns.allianz.com.ar/CommonCore/EBM"/>
         <ebm:ErrorCode xmlns:ebm="http://xmlns.allianz.com.ar/CommonCore/EBM"/>
         <cot:DataArea>
            <cot:CalcularCotizacionFullVehiculoResponse>
               <cot:numeroDeCotizacion>23866865</cot:numeroDeCotizacion>
               <cot:ListaCotizacionFull>
                  <cot1:CotizacionFull xmlns:cot1="http://xmlns.allianz.com.ar/Core/EBO/Allianz/CotizacionFullVehiculo">
                     <cot1:codigoDeCobertura>58</cot1:codigoDeCobertura>
                     <cot1:descripcionDeCobertura>C2 - DEST.TOTAL INC. Y ROBO PARC. Y TOTAL</cot1:descripcionDeCobertura>
                     <cot1:codigoDeProducto>52</cot1:codigoDeProducto>
                     <cot1:descripcionDeProducto>CLASICA SEGMENTADA</cot1:descripcionDeProducto>
                     <cot1:prima>
                        <prim:importePrima xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">2043.83</prim:importePrima>
                        <prim:importePrimaRC xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">786.88</prim:importePrimaRC>
                        <prim:importePrimaCasco xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">1131</prim:importePrimaCasco>
                        <prim:importePrimaAccesorio xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">125.95</prim:importePrimaAccesorio>
                        <prim:importePrimaSinFranquicia xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">0</prim:importePrimaSinFranquicia>
                        <prim:importePrimaServicios xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">0</prim:importePrimaServicios>
                     </cot1:prima>
                     <cot1:premio>
                        <prem:importePremio xmlns:prem="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Premio">2517.99</prem:importePremio>
                     </cot1:premio>
                     <cot1:impuestos>
                        <imp:importeSellados xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">0</imp:importeSellados>
                        <imp:porcentajeIVA xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">21</imp:porcentajeIVA>
                        <imp:importeIVA xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">429.2</imp:importeIVA>
                        <imp:importeIngresosBrutos xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">0</imp:importeIngresosBrutos>
                        <imp:importeTotalImpuestos xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">44.96</imp:importeTotalImpuestos>
                     </cot1:impuestos>
                     <cot1:sumaAsegurada>179600</cot1:sumaAsegurada>
                     <cot1:porcentajeRecargoFinanciero>0</cot1:porcentajeRecargoFinanciero>
                     <cot1:importeRecargoFinanciero>0</cot1:importeRecargoFinanciero>
                     <cot1:requiereInspeccion>true</cot1:requiereInspeccion>
                     <cot1:ListaFranquicias/>
                  </cot1:CotizacionFull>
                  <cot1:CotizacionFull xmlns:cot1="http://xmlns.allianz.com.ar/Core/EBO/Allianz/CotizacionFullVehiculo">
                     <cot1:codigoDeCobertura>59</cot1:codigoDeCobertura>
                     <cot1:descripcionDeCobertura>D2 - TODO RIESGO CON FRANQUICIA FIJA</cot1:descripcionDeCobertura>
                     <cot1:codigoDeProducto>52</cot1:codigoDeProducto>
                     <cot1:descripcionDeProducto>CLASICA SEGMENTADA</cot1:descripcionDeProducto>
                     <cot1:prima>
                        <prim:importePrima xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">3150.93</prim:importePrima>
                        <prim:importePrimaRC xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">786.88</prim:importePrimaRC>
                        <prim:importePrimaCasco xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">2127.17</prim:importePrimaCasco>
                        <prim:importePrimaAccesorio xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">236.88</prim:importePrimaAccesorio>
                        <prim:importePrimaSinFranquicia xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">0</prim:importePrimaSinFranquicia>
                        <prim:importePrimaServicios xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">0</prim:importePrimaServicios>
                     </cot1:prima>
                     <cot1:premio>
                        <prem:importePremio xmlns:prem="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Premio">3881.95</prem:importePremio>
                     </cot1:premio>
                     <cot1:impuestos>
                        <imp:importeSellados xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">0</imp:importeSellados>
                        <imp:porcentajeIVA xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">21</imp:porcentajeIVA>
                        <imp:importeIVA xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">661.7</imp:importeIVA>
                        <imp:importeIngresosBrutos xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">0</imp:importeIngresosBrutos>
                        <imp:importeTotalImpuestos xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">69.32</imp:importeTotalImpuestos>
                     </cot1:impuestos>
                     <cot1:sumaAsegurada>179600</cot1:sumaAsegurada>
                     <cot1:porcentajeRecargoFinanciero>0</cot1:porcentajeRecargoFinanciero>
                     <cot1:importeRecargoFinanciero>0</cot1:importeRecargoFinanciero>
                     <cot1:requiereInspeccion>true</cot1:requiereInspeccion>
                     <cot1:ListaFranquicias>
                        <cot1:Franquicia>
                           <cot1:codigoTipoFranquicia>F</cot1:codigoTipoFranquicia>
                           <cot1:valorFranquicia>6000</cot1:valorFranquicia>
                        </cot1:Franquicia>
                     </cot1:ListaFranquicias>
                  </cot1:CotizacionFull>
                  <cot1:CotizacionFull xmlns:cot1="http://xmlns.allianz.com.ar/Core/EBO/Allianz/CotizacionFullVehiculo">
                     <cot1:codigoDeCobertura>76</cot1:codigoDeCobertura>
                     <cot1:descripcionDeCobertura>C4 - DEST.TOTAL INC. Y ROBO PARC. Y TOTAL</cot1:descripcionDeCobertura>
                     <cot1:codigoDeProducto>53</cot1:codigoDeProducto>
                     <cot1:descripcionDeProducto>PLUS SEGMENTADA</cot1:descripcionDeProducto>
                     <cot1:prima>
                        <prim:importePrima xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">2075.71</prim:importePrima>
                        <prim:importePrimaRC xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">796.97</prim:importePrimaRC>
                        <prim:importePrimaCasco xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">1150.61</prim:importePrimaCasco>
                        <prim:importePrimaAccesorio xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">128.13</prim:importePrimaAccesorio>
                        <prim:importePrimaSinFranquicia xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">0</prim:importePrimaSinFranquicia>
                        <prim:importePrimaServicios xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">0</prim:importePrimaServicios>
                     </cot1:prima>
                     <cot1:premio>
                        <prem:importePremio xmlns:prem="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Premio">2557.28</prem:importePremio>
                     </cot1:premio>
                     <cot1:impuestos>
                        <imp:importeSellados xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">0</imp:importeSellados>
                        <imp:porcentajeIVA xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">21</imp:porcentajeIVA>
                        <imp:importeIVA xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">435.9</imp:importeIVA>
                        <imp:importeIngresosBrutos xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">0</imp:importeIngresosBrutos>
                        <imp:importeTotalImpuestos xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">45.67</imp:importeTotalImpuestos>
                     </cot1:impuestos>
                     <cot1:sumaAsegurada>179600</cot1:sumaAsegurada>
                     <cot1:porcentajeRecargoFinanciero>0</cot1:porcentajeRecargoFinanciero>
                     <cot1:importeRecargoFinanciero>0</cot1:importeRecargoFinanciero>
                     <cot1:requiereInspeccion>true</cot1:requiereInspeccion>
                     <cot1:ListaFranquicias/>
                  </cot1:CotizacionFull>
                  <cot1:CotizacionFull xmlns:cot1="http://xmlns.allianz.com.ar/Core/EBO/Allianz/CotizacionFullVehiculo">
                     <cot1:codigoDeCobertura>77</cot1:codigoDeCobertura>
                     <cot1:descripcionDeCobertura>D4 - TODO RIESGO CON FRANQUICIA FIJA PLAT</cot1:descripcionDeCobertura>
                     <cot1:codigoDeProducto>53</cot1:codigoDeProducto>
                     <cot1:descripcionDeProducto>PLUS SEGMENTADA</cot1:descripcionDeProducto>
                     <cot1:prima>
                        <prim:importePrima xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">3172.19</prim:importePrima>
                        <prim:importePrimaRC xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">796.97</prim:importePrimaRC>
                        <prim:importePrimaCasco xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">2137.22</prim:importePrimaCasco>
                        <prim:importePrimaAccesorio xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">238</prim:importePrimaAccesorio>
                        <prim:importePrimaSinFranquicia xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">0</prim:importePrimaSinFranquicia>
                        <prim:importePrimaServicios xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">0</prim:importePrimaServicios>
                     </cot1:prima>
                     <cot1:premio>
                        <prem:importePremio xmlns:prem="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Premio">3908.13</prem:importePremio>
                     </cot1:premio>
                     <cot1:impuestos>
                        <imp:importeSellados xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">0</imp:importeSellados>
                        <imp:porcentajeIVA xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">21</imp:porcentajeIVA>
                        <imp:importeIVA xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">666.16</imp:importeIVA>
                        <imp:importeIngresosBrutos xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">0</imp:importeIngresosBrutos>
                        <imp:importeTotalImpuestos xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">69.78</imp:importeTotalImpuestos>
                     </cot1:impuestos>
                     <cot1:sumaAsegurada>179600</cot1:sumaAsegurada>
                     <cot1:porcentajeRecargoFinanciero>0</cot1:porcentajeRecargoFinanciero>
                     <cot1:importeRecargoFinanciero>0</cot1:importeRecargoFinanciero>
                     <cot1:requiereInspeccion>true</cot1:requiereInspeccion>
                     <cot1:ListaFranquicias>
                        <cot1:Franquicia>
                           <cot1:codigoTipoFranquicia>F</cot1:codigoTipoFranquicia>
                           <cot1:valorFranquicia>6000</cot1:valorFranquicia>
                        </cot1:Franquicia>
                     </cot1:ListaFranquicias>
                  </cot1:CotizacionFull>
                  <cot1:CotizacionFull xmlns:cot1="http://xmlns.allianz.com.ar/Core/EBO/Allianz/CotizacionFullVehiculo">
                     <cot1:codigoDeCobertura>79</cot1:codigoDeCobertura>
                     <cot1:descripcionDeCobertura>D4 - TODO RIESGO CON FRANQUICIA SOBRE VALOR OKM</cot1:descripcionDeCobertura>
                     <cot1:codigoDeProducto>53</cot1:codigoDeProducto>
                     <cot1:descripcionDeProducto>PLUS SEGMENTADA</cot1:descripcionDeProducto>
                     <cot1:prima>
                        <prim:importePrima xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">2562.44</prim:importePrima>
                        <prim:importePrimaRC xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">796.97</prim:importePrimaRC>
                        <prim:importePrimaCasco xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">1588.57</prim:importePrimaCasco>
                        <prim:importePrimaAccesorio xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">176.9</prim:importePrimaAccesorio>
                        <prim:importePrimaSinFranquicia xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">0</prim:importePrimaSinFranquicia>
                        <prim:importePrimaServicios xmlns:prim="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Prima">0</prim:importePrimaServicios>
                     </cot1:prima>
                     <cot1:premio>
                        <prem:importePremio xmlns:prem="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Premio">3156.91</prem:importePremio>
                     </cot1:premio>
                     <cot1:impuestos>
                        <imp:importeSellados xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">0</imp:importeSellados>
                        <imp:porcentajeIVA xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">21</imp:porcentajeIVA>
                        <imp:importeIVA xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">538.11</imp:importeIVA>
                        <imp:importeIngresosBrutos xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">0</imp:importeIngresosBrutos>
                        <imp:importeTotalImpuestos xmlns:imp="http://xmlns.allianz.com.ar/Core/EBO/Allianz/Impuesto">56.36</imp:importeTotalImpuestos>
                     </cot1:impuestos>
                     <cot1:sumaAsegurada>179600</cot1:sumaAsegurada>
                     <cot1:porcentajeRecargoFinanciero>0</cot1:porcentajeRecargoFinanciero>
                     <cot1:importeRecargoFinanciero>0</cot1:importeRecargoFinanciero>
                     <cot1:requiereInspeccion>true</cot1:requiereInspeccion>
                     <cot1:ListaFranquicias>
                        <cot1:Franquicia>
                           <cot1:codigoTipoFranquicia>P</cot1:codigoTipoFranquicia>
                           <cot1:valorFranquicia>3</cot1:valorFranquicia>
                        </cot1:Franquicia>
                     </cot1:ListaFranquicias>
                  </cot1:CotizacionFull>
               </cot:ListaCotizacionFull>
            </cot:CalcularCotizacionFullVehiculoResponse>
         </cot:DataArea>
      </cot:CalcularCotizacionFullVehiculoResponseEBM>
   </soapenv:Body>
</soapenv:Envelope>